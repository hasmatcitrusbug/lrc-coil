
		function global_validate() {

			var global_val = true;

			//alert('got here 1');
			var column_count = document.getElementById("column_count").value;

			//alert('column count = ' + column_count);

			//alert('got here 2');
			for (i = 1; i <= column_count; i++) {

				global_val = validate(i);

			}

			return global_val;
		}

		function save_single_calc_results() {
			document.getElementById("single_calc_action").value = "1";
			document.getElementById("custom_command_form").submit();
		}

		function load_calc_id(the_id, part_no) {

			if (confirm("Are you sure you want to load this run? Your existing data on the page will be lost if you haven't saved it.")) {

				document.getElementById("save_part_no").value = part_no;
				document.getElementById("single_calc_action").value = "2";
				document.getElementById("calc_id").value = the_id;
				document.getElementById("custom_command_form").submit();
			}
		}

		function delete_calc_id(the_id, the_name, part_no) {

			/*
				alert("THE ID = " + the_id);
				alert("THE NAME = " + the_name);
				alert("PART NO = " + part_no);
			//*/

			if (confirm("Are you sure you want to delete this run? It will be lost along with your existing data on the page if you haven't already saved it.")) {

				document.getElementById("save_part_no").value = part_no;
				document.getElementById("single_calc_action").value = "3";
				document.getElementById("calc_id").value = the_id;
				document.getElementById("calc_name").value = the_name;
				document.getElementById("custom_command_form").submit();
			}
		}

		function load_id(the_id) {

			if (confirm("Are you sure you want to load this run? Your existing data on the page will be lost if you haven't saved it.")) {

				document.getElementById("command_action").value = "3";
				document.getElementById("load_id").value = the_id;
				document.getElementById("command_form").submit();
			}
		}

		function delete_id(the_id) {

			if (confirm("Are you sure you want to delete this run? It will be lost along with your existing data on the page if you haven't saved it.")) {

				document.getElementById("command_action").value = "2";
				document.getElementById("delete_id").value = the_id;
				document.getElementById("command_form").submit();
			}
		}

		function save_this_run() {

			if (confirm('Ready to save this run?')) {

				//var valid = global_validate();

				//alert("VALID = " + valid);

				//if (valid == true) {

				//alert("Validation passed! Please click 'OK' to run the calculations.");

				document.getElementById("special_action").value = "2";
				document.getElementById("editable_form").submit();
				//}
			}

		}

		function submit_editable_form() {

			//alert ("GOT HERE!!!");

			if (confirm('Are you ready to calculate these variables?')) {

				//alert('before validation');
				var valid = global_validate();

				//alert("VALID = " + valid);

				//alert('after validation');
				if (valid == true) {

					//alert("Validation passed! Please click 'OK' to run the calculations.");

					//alert('before special action');
					document.getElementById("special_action").value = "1";

					//alert('before submitting the form');
					document.getElementById("editable_form").submit();
				}
			}

		}

		function submit_form() {

			if (confirm('Are you sure you want to calculate these variables?')) {

				document.getElementById("main_form").submit();
			}
		}


		// ***********************
		// * PHP TEMPLATE FILE 1 *
		// ***********************

		function test_data(number) {

			//alert ("GOT HERE TEST DATA CW!!!");

			if (number == 1) {

				document.getElementById("the_title").value = "14CW-2-4-6-10/C16-1";
				document.getElementById("input_CHO").value = "R";
				document.getElementById("input_CTYP").value = "14";
				document.getElementById("input_ROW").value = "4";
				document.getElementById("input_CH").value = "24";
				document.getElementById("input_CL").value = "24";
				document.getElementById("input_FPI").value = "12";
				document.getElementById("input_DBE").value = "80";
				document.getElementById("input_WBE").value = "67";
				//document.getElementById("input_OD").value = "1*2";
				document.getElementById("input_TT").value = "0.016";
				document.getElementById("input_DP").value = "23";
				document.getElementById("input_CONN").value = "STD";
				document.getElementById("input_CIR").value = "0";
				document.getElementById("input_YF").value = "0.006";
				document.getElementById("input_WE").value = "45";
				document.getElementById("input_TTYP").value = "G";
				document.getElementById("input_TM").value = "CU";
				document.getElementById("input_ODDF").value = "N";
				document.getElementById("input_GPM").value = "0";
				document.getElementById("input_FFO").value = "0";
				document.getElementById("input_CG").value = "100";
				document.getElementById("input_FL").value = "WTR";
				document.getElementById("input_FM").value = "AL";
				document.getElementById("input_TURB").value = "N";
				document.getElementById("input_CQTY").value = "1";
				document.getElementById("input_FTYP").value = "C";
				document.getElementById("input_FFI").value = "0";
				document.getElementById("input_ALTD").value = "0";
				document.getElementById("input_CFMT").value = "SCFM";
				document.getElementById("input_CFM").value = "2000";
				document.getElementById("input_DT").value = "10";

				/*
				//document.getElementById("input_UVG").value = "0";
				document.getElementById("input_CPG").value = "0";
				document.getElementById("input_DPG").value = "0";
				document.getElementById("input_TKG").value = "0";
				document.getElementById("input_DBL").value = "0";
				document.getElementById("input_SH").value = "0";
				document.getElementById("input_WBL").value = "0";
				document.getElementById("input_TH").value = "0";
				//*/
			}

		}

		function clear_data() {

			document.getElementById("the_title").value = "";
			document.getElementById("input_CHO").value = "";
			document.getElementById("input_CTYP").value = "";
			document.getElementById("input_ROW").value = "";
			document.getElementById("input_CH").value = "";
			document.getElementById("input_CL").value = "";
			document.getElementById("input_FPI").value = "";
			document.getElementById("input_DBE").value = "";
			document.getElementById("input_WBE").value = "";
			//document.getElementById("input_OD").value ="";	
			document.getElementById("input_TT").value = "";
			document.getElementById("input_DP").value = "";
			document.getElementById("input_CONN").value = "";
			document.getElementById("input_CIR").value = "";
			document.getElementById("input_YF").value = "";
			document.getElementById("input_WE").value = "";
			document.getElementById("input_TTYP").value = "";
			document.getElementById("input_TM").value = "";
			document.getElementById("input_ODDF").value = "";
			document.getElementById("input_GPM").value = "";
			document.getElementById("input_FFO").value = "";
			document.getElementById("input_CG").value = "";
			document.getElementById("input_FL").value = "";
			document.getElementById("input_FM").value = "";
			document.getElementById("input_TURB").value = "";
			document.getElementById("input_CQTY").value = "";
			document.getElementById("input_FTYP").value = "";
			document.getElementById("input_FFI").value = "";
			document.getElementById("input_ALTD").value = "";
			document.getElementById("input_CFMT").value = "";
			document.getElementById("input_CFM").value = "";
			document.getElementById("input_DT").value = "";
			/*
			//document.getElementById("input_UVG").value ="";	
			document.getElementById("input_CPG").value ="";	
			document.getElementById("input_DPG").value ="";	
			document.getElementById("input_TKG").value ="";	
			document.getElementById("input_DBL").value ="";	
			document.getElementById("input_SH").value ="";	
			document.getElementById("input_WBL").value ="";	
			document.getElementById("input_TH").value ="";	
			//*/
		}

		function clear_results() {

			//alert ("CLEAR 1");
			document.getElementById("output_QAZ").value = "";
			//alert ("CLEAR 10");
			document.getElementById("output_SHAZ").value = "";
			//alert ("CLEAR 11");
			document.getElementById("output_AT2Z").value = "";
			//alert ("CLEAR 12");
			document.getElementById("output_WBLAZ").value = "";
			//alert ("CLEAR 13");
			document.getElementById("output_COILWT").value = "";
			//alert ("CLEAR 14");
			document.getElementById("output_CIRZ").value = "";
			//alert ("CLEAR 15");
			document.getElementById("output_FA").value = "";
			//alert ("CLEAR 16");
			document.getElementById("output_DPAJZ").value = "";
			//alert ("CLEAR 17");
			//document.getElementById("output_VER").value = "";
			document.getElementById("output_VWZ").value = "";
			//alert ("CLEAR 18");
			document.getElementById("output_FPIZ").value = "";
			//alert ("CLEAR 19");
			document.getElementById("output_AWLZ").value = "";
			//alert ("CLEAR 20");
			document.getElementById("output_AFAV").value = "";
			//alert ("CLEAR 21");
			document.getElementById("output_DP1Z").value = "";
			//alert ("CLEAR 22");
			document.getElementById("output_GPMZ").value = "";
			//alert ("CLEAR 23");
			document.getElementById("output_VWZ").value = "";
			//alert ("CLEAR 24");
			//document.getElementById("output_CQTYZ").value = "";
			//alert ("CLEAR 25");
			document.getElementById("output_WBLAZ").value = "";
			//alert ("CLEAR 26");
			document.getElementById("output_CONSZ").value = "";
			//alert ("CLEAR 27");
			document.getElementById("output_ROWZ").value = "";
			//alert ("CLEAR 28");
			document.getElementById("output_FLUID").value = "";
			//alert ("CLEAR 29");
			document.getElementById("output_REL").value = "";
			//alert ("CLEAR 30");
			document.getElementById("output_SERZ").value = "";
			//alert ("CLEAR 31");
			document.getElementById("output_QS").value = "";
			//alert ("CLEAR 32");
			document.getElementById("output_SDP").value = "";
			//alert ("CLEAR DONE!!!");

		}

		function test_refresh(id_index) {

			var series_val = document.getElementById("series" + id_index).value;
			var ctyp_derived = series_val.substring(0, 2).trim();

			//alert("CTYP inferred: " + ctyp_derived);

			document.getElementById("ctyp" + id_index).value = ctyp_derived;
			ctyp_refresh(id_index);
			tt_changed(id_index);

		}

		function series_changed(id_index) {

			var series_val = document.getElementById("series" + id_index).value;
			var ctyp_derived = series_val.substring(0, 2).trim();

			//alert("ID Index: " + id_index);
			//alert("CTYP inferred: " + ctyp_derived);

			document.getElementById("ctyp" + id_index).value = ctyp_derived;

			//alert("CTYP done!");

			ctyp_refresh(id_index);
			tt_changed(id_index);
			validate_ft(id_index);
		}

		function tt_changed(id_index) {

			var ttyp_val;
			var tt_val = document.getElementById("tt" + id_index).value;

			if (tt_val.toLowerCase().indexOf("rifled") >= 0) {
				ttyp_val = "R";
			} else {
				ttyp_val = "S";
			}

			document.getElementById("ttyp" + id_index).value = ttyp_val;
			//document.getElementById("tthelp" + id_index).value = tt_val;

		}

		function cfmt_updated(id_index) {
			var cfmt_val = document.getElementById("cfmt" + id_index).value;
			document.getElementById("cfmt" + id_index).value = cfmt_val;
		}

		function tt_updated(id_index) {

			var tt_val = document.getElementById("tt" + id_index).value;
			//alert("TT" + id_index + " UPDATED: " + tt_val);
			var tt_first_letter = tt_val.charAt(0);
			//alert("TT" + id_index + " FIRST LETTER: " + tt_first_letter);

			var tt_remainder = tt_val.substr(1);

			//alert("TT" + id_index + " REMAINDER: " + tt_remainder);

			document.getElementById("ttyp" + id_index).value = tt_first_letter;
		}

		function ctyp_refresh(id_index) {

			var module_name = "CW";

			//alert("ctyp test 1");

			var opt;
			var el;
			var opt2;
			var el2;
			var opt3;
			var el3;
			var val3;


			var ctyp_val = document.getElementById("ctyp" + id_index).value;
			var od_val = 0;
			//var ftyp_val = "C";
			var the_i = 0;

			var the_options = ["empty array"];
			var the_values = ["empty array"];

			opt = "SS";
			el = document.createElement("option");
			el.textContent = opt;
			el.value = opt;

			opt2 = "SS";
			el2 = document.createElement("option");
			el2.textContent = opt2;
			el2.value = opt2;


			//alert("ctyp val = " + ctyp_val);
			//alert("ID INDEX = " + id_index);



			if (ctyp_val == 5) {

				od_val = "5mm";
				ftyp_val = "C";
				the_options = ["5mm x .008 min wall rifled, CU", "5mm x .020 smooth, CU", "5mm x .028 smooth, SS"];
				the_values = ["R0.008", "S0.020", "S0.028"];

				// Adjust the Tube Material (TM) dropdown
				var the_tm = document.getElementById("tm" + id_index);
				the_tm.appendChild(el);

				// Adjust the Fin Material (FM) dropdown
				var the_fm = document.getElementById("fm" + id_index);
				the_fm.appendChild(el2);


			} else if (ctyp_val == 6) {

				//alert("ctyp 6 starting");

				od_val = "5mm";
				ftyp_val = "L";
				the_options = ["5mm x .008 min wall rifled, CU", "5mm x .020 smooth, CU", "5mm x .028 smooth, SS"];
				the_values = ["R0.008", "S0.020", "S0.028"];

				// Adjust the Tube Material (TM) dropdown
				var the_tm = document.getElementById("tm" + id_index);
				the_tm.appendChild(el);

				// Adjust the Fin Material (FM) dropdown
				var the_fm = document.getElementById("fm" + id_index);
				the_fm.appendChild(el2);

			} else if (ctyp_val == 25) {
				od_val = "5/16";
				ftyp_val = "C";
				the_options = ["5/16 x .012 min wall rifled, CU"];
				the_values = ["R0.012"];
			} else if (ctyp_val == 35) {
				od_val = "3/8";
				ftyp_val = "L";
				the_options = ["3/8 x .012 min wall rifled, CU", "3/8 x .016 min wall rifled, CU", "3/8 x .014 wall smooth, CU", "3/8 x .020 smooth, CU", "3/8 x .028 smooth, CU"];
				the_values = ["R0.012", "R0.016", "S0.014", "S0.020", "S0.028"];
			} else if (ctyp_val == 36) {
				od_val = "3/8";
				ftyp_val = "C";
				the_options = ["3/8 x .012 min wall rifled, CU", "3/8 x .016 min wall rifled, CU", "3/8 x .014 wall smooth, CU", "3/8 x .020 smooth, CU", "3/8 x .028 smooth, CU"];
				the_values = ["R0.012", "R0.016", "S0.014", "S0.020", "S0.028"];
			} else if (ctyp_val == 37) {
				od_val = "3/8";
				ftyp_val = "C";
				the_options = ["3/8 x .012 min wall rifled, CU", "3/8 x .016 min wall rifled, CU", "3/8 x .014 wall smooth, CU", "3/8 x .020 smooth, CU", "3/8 x .028 smooth, CU"];
				the_values = ["R0.012", "R0.016", "S0.014", "S0.020", "S0.028"];
			} else if (ctyp_val == 38) {
				od_val = "3/8";
				ftyp_val = "C";
				the_options = ["3/8 x .012 min wall rifled, CU", "3/8 x .016 min wall rifled, CU", "3/8 x .014 wall smooth, CU", "3/8 x .020 smooth, CU", "3/8 x .028 smooth, CU"];
				the_values = ["R0.012", "R0.016", "S0.014", "S0.020", "S0.028"];
			} else if (ctyp_val == 13) {
				od_val = "1/2";
				ftyp_val = "C";
				the_options = ["1/2 x .016 wall smooth, CU", "1/2 x .016 min wall rifled, CU"];
				the_values = ["S0.016", "R0.016"];
			} else if (ctyp_val == 14) {
				od_val = "1/2";
				ftyp_val = "C";
				the_options = ["1/2 x .016 wall smooth, CU", "1/2 x .016 min wall rifled, CU"];
				the_values = ["S0.016", "R0.016"];
			} else if (ctyp_val == 15) {
				od_val = "1/2";
				ftyp_val = "L";
				the_options = ["1/2 x .016 wall smooth, CU", "1/2 x .016 min wall rifled, CU"];
				the_values = ["S0.016", "R0.016"];
			} else if (ctyp_val == 57) {
				od_val = "5/8";
				ftyp_val = "C";
				the_options = ["5/8 x .018 wall smooth, CU", "5/8 x .025 wall smooth, CU", "5/8 x .035 wall smooth, CU"];
				the_values = ["S0.018", "S0.025", "S0.035"];
			} else if (ctyp_val == 59) {
				od_val = "5/8";
				ftyp_val = "C";
				the_options = ["5/8 x .018 wall smooth, CU", "5/8 x .025 wall smooth, CU", "5/8 x .035 wall smooth, CU"];
				the_values = ["S0.018", "S0.025", "S0.035"];
			}



			var the_select = document.getElementById("tt" + id_index);
			var the_length = the_options.length;

			//alert("THE OPTIONS.LENGTH = " + the_length);

			//alert("ctyp test 4");

			if (the_length > 0) {

				// First clear the select
				for (the_i = the_select.options.length - 1; the_i >= 0; the_i--) {
					the_select.remove(the_i);
				}

				//*
				for (the_i = 0; the_i < the_length; the_i++) {

					opt3 = the_options[the_i];
					val3 = the_values[the_i];
					el3 = document.createElement("option");
					el3.textContent = opt3;
					el3.value = val3;
					the_select.appendChild(el3);

				}
				//*/
			}

			//alert("ctyp test 5");

			//alert("!!!GOT HERE 2!!!");


			//document.getElementById("ftyp" + id_index).value = ftyp_val;	

			//alert("ctyp test 6");

			//document.getElementById("od" + id_index).value = od_val;	

			//alert("ctyp test 7");
		}

		function validate_ft(id_index) {

			var test_ctyp = document.getElementById("ctyp" + id_index).value;
			var test_ch = document.getElementById("ch" + id_index).value;

			//alert("!!!GOT HERE FT 1!!!");

			// FT validation
			var test_ft = 0;
			var test_remainder = 0;
			var test_divisor = 0;
			if ((test_ctyp == 5) || (test_ctyp == 6)) {
				test_divisor = 0.75;
			} else if ((test_ctyp == 35) || (test_ctyp == 36) || (test_ctyp == 37) || (test_ctyp == 38)) {
				test_divisor = 1;
			} else if ((test_ctyp == 13) || (test_ctyp == 14) || (test_ctyp == 15) || (test_ctyp == 57)) {
				test_divisor = 1.5;
			} else if ((test_ctyp == 59)) {
				test_divisor = 1.75;
			}

			test_ft = test_ch / test_divisor;
			test_remainder = test_ch % test_divisor;

			//alert("!!!GOT HERE FT 2!!!");

			if (test_remainder > 0) {
				validation_failed = true;
				//alert("FT Validation failed! \r\n FT = " + test_ft + "\r\n CH = " + test_ch + "\r\n CTYP = " + test_ctyp + "\r\n Divisor = " + test_divisor + "\r\n Remainder = " + test_remainder + "\r\n Please adjust the CH value or the CTYP value accordingly.");
			} else {
				//alert("FT Validation passed! \r\n FT = " + test_ft + "\r\n CH = " + test_ch + "\r\n CTYP = " + test_ctyp + "\r\n Divisor = " + test_divisor + "\r\n Remainder = " + test_remainder);
			}

			//alert("!!!GOT HERE FT 3!!!");

			document.getElementById("ft" + id_index).value = test_ft;

		}

		function validate(id_index) {

			var validation_failed = false;

			//alert("GOT HERE 0");

			//alert("valid test 1");
			//alert("valid test TEST");


			// DESC
			var valid_series = document.getElementById("series" + id_index).value;
			//alert("valid test Q123");
			//var valid_tthelp = document.getElementById("tthelp" + id_index).value;
			//alert("valid test Q234");
			//var valid_desc = document.getElementById("description" + id_index).value;
			//alert("valid test Q345");
			var valid_cho = document.getElementById("cho" + id_index).value;
			//alert("valid test Q11");
			var valid_ch = document.getElementById("ch" + id_index).value;
			//alert("valid test Q12");
			var valid_cl = document.getElementById("cl" + id_index).value;
			//alert("valid test Q13");
			var valid_cfm = document.getElementById("cfm" + id_index).value;
			//alert("valid test Q2");
			var valid_dbe = document.getElementById("dbe" + id_index).value;
			var valid_wbe = document.getElementById("wbe" + id_index).value;
			//alert("valid test Q20");
			//var valid_cond = document.getElementById("cond" + id_index).value;
			//alert("valid test Q21");
			//var valid_suct = document.getElementById("suct" + id_index).value;
			//alert("valid test Q22");
			var valid_fl = document.getElementById("fl" + id_index).value;
			//alert("valid test Q3");
			var valid_cir = document.getElementById("cir" + id_index).value;
			//alert("valid test Q30");
			var valid_fpi = document.getElementById("fpi" + id_index).value;
			//alert("valid test Q31");
			var valid_fm = document.getElementById("fm" + id_index).value;
			//alert("valid test Q4");
			var valid_yf = document.getElementById("yf" + id_index).value;
			var valid_tm = document.getElementById("tm" + id_index).value;
			var valid_row = document.getElementById("row" + id_index).value;
			var valid_tt = document.getElementById("tt" + id_index).value;
			//alert("valid test Q5");
			var valid_ctyp = document.getElementById("ctyp" + id_index).value;
			var valid_ffo = document.getElementById("ffo" + id_index).value;
			var valid_cfmt = document.getElementById("cfmt" + id_index).value;
			var valid_altd = document.getElementById("altd" + id_index).value;
			//alert("valid test Q6");
			var valid_ttyp = document.getElementById("ttyp" + id_index).value;
			var valid_ftyp = document.getElementById("ftyp" + id_index).value;
			//var valid_mbh = document.getElementById("mbh" + id_index).value;
			//var valid_sb = document.getElementById("sb" + id_index).value;
			//alert("valid test Q7");
			//var valid_scir = document.getElementById("scir" + id_index).value;
			//alert("valid test Q70");
			//var valid_ft = document.getElementById("ft" + id_index).value;
			//alert("valid test Q71");
			var valid_od = document.getElementById("od" + id_index).value;
			//alert("valid test Q72");
			var valid_facedim = document.getElementById("facedim" + id_index).value;
			//alert("valid test Q73");
			var valid_coilpartno = document.getElementById("coilpartno" + id_index).value;
			//alert("valid test Q74");
			//var valid_calc = document.getElementById("calc" + id_index).value;
			var valid_dp = document.getElementById("dp" + id_index).value;
			//alert("valid test Q75");
			var valid_conn = document.getElementById("conn" + id_index).value;
			//alert("valid test Q76");
			var valid_we = document.getElementById("we" + id_index).value;
			//alert("valid test Q77");
			var valid_oddf = document.getElementById("oddf" + id_index).value;
			var valid_gpm = document.getElementById("gpm" + id_index).value;
			var valid_turb = document.getElementById("turb" + id_index).value;
			var valid_cg = document.getElementById("cg" + id_index).value;
			var valid_cqty = document.getElementById("cqty" + id_index).value;
			var valid_ffi = document.getElementById("ffi" + id_index).value;
			//alert("valid test Q78");
			var valid_dt = document.getElementById("dt" + id_index).value;
			//alert("valid test Q79");

			// *************************
			// * JAVASCRIPT VALIDATION * 
			// *************************

			ctyp_refresh(id_index);

			//alert("valid test 3");

			//alert("GOT HERE 1");

			// Description
			//var test_desc = IsAlphaNumeric(valid_desc);
			/*
				if (test_desc == false) {
				validation_failed = true;
				//alert("The description string is not alphanumeric.");	
				}
			*/

			/*
			if ((valid_desc.length < 1) || (valid_desc.length > 50)) {
				validation_failed = true;
				//alert("The description string is shorter than 1 or longer than 50 characters.");
			}
			//*/

			//alert("valid test 4");

			//alert("GOT HERE 2");

			//*
			// Coil Part #

			//alert("GOT HERE 2");
			var tt_derived;
			var tt_loc;
			tt_loc = valid_tt.indexOf(".0");

			//alert("valid test 5");

			//alert("VALID TT = " + valid_tt);
			//alert("TT LOC = " + tt_loc);

			tt_derived = valid_tt.substr((tt_loc + 2), 2);

			//alert("valid test 50");

			//alert("TT DERIVED = " + tt_derived);

			var module_name;

			//alert("valid test 51");
			module_name = "CW";

			//alert("valid test 52");

			//alert("VALID CTYP = " + valid_ctyp);
			//alert("MODULE NAME = " + module_name);
			//alert("VALID ROW = " + valid_row);
			////alert("VALID FT = " + valid_ft);
			//alert("VALID FPI = " + valid_fpi);
			//alert("VALID TM = " + valid_tm);
			//alert("VALID CHO = " + valid_cho);
			//alert("VALID TT DERIVED = " + tt_derived);
			//alert("VALID CIR = " + valid_cir);


			var test_coilpartno = valid_ctyp + module_name + valid_row + "-" + valid_fpi + "/" + valid_tm.charAt(0) + valid_cho + tt_derived + "-" + valid_cir;
			//var test_coilpartno = valid_ctyp + module_name + valid_row + "-" + valid_ft + "-" + valid_fpi + "/" + valid_tm.charAt(0) + valid_cho + tt_derived + "-" + valid_cir;
			//alert("GOT HERE 3");

			//alert("valid test 53");

			//*
			if (test_coilpartno != valid_coilpartno) {
				valid_coilpartno = test_coilpartno;
			}
			//*/

			//alert("valid test 54");		

			// Face dimensions
			//*
			if (valid_facedim == "") {
				valid_facedim = valid_ch + "x" + valid_cl;
			}
			//*/
			//alert("GOT HERE 5");

			//alert("valid test 6");

			// CL = Fin length (inches) 
			//alert("TESTING CL!!!");  
			var test_cl = IsNumeric(valid_cl);
			if (test_cl == false) {
				validation_failed = true;
				//alert("The fin length value is not numeric.");	
			}

			//alert("valid test 7");

			// FPI
			var test_fpi = IsNumeric(valid_fpi);
			if (test_fpi == false) {
				validation_failed = true;
				//alert("The FPI value is not numeric.");
			} else {

				if ((valid_cfmt.parseInt < 1) || (valid_cfmt.parseInt > 22)) {
					validation_failed = true;
					//alert("The FPI value is lower than 1 or higher than 22.");
				}
			}

			//alert("valid test 8");

			// DBE = Ent. DB (F)
			//alert("TESTING DBE!!!");  
			var test_dbe = IsNumeric(valid_dbe);
			if (test_dbe == false) {
				validation_failed = true;
				//alert("The DBE value is not numeric.");
			}

			//alert("valid test 9");

			// CIR
			var test_cir = IsNumeric(valid_cir);
			if (test_cir == false) {
				validation_failed = true;
				//alert("The cir value is not numeric.");
			} else {

				if ((valid_cfmt.parseInt < 0) || (valid_cfmt.parseInt > 250)) {
					validation_failed = true;
					//alert("The CIR value is lower than 0 or higher than 250.");
				}
			}

			//alert("valid test 10");

			/*
			// SUCT
			var test_suct = IsNumeric(valid_suct);
			if (test_suct == false) {
				validation_failed = true;
				//alert("The SUCT value is not numeric.");
				} else {
				
				if ((valid_cfmt.parseInt < -60) || (valid_cfmt.parseInt > 150)) {
					validation_failed = true;
					//alert("The SUCT value is lower than -60 or higher than 150.");
				}		
			}
			//*/

			//alert("valid test 11");

			/*
			// COND
			var test_cond = IsNumeric(valid_cond);
			if (test_cond == false) {
				validation_failed = true;
				//alert("The COND value is not numeric.");
				} else {
				
				if ((valid_cfmt.parseInt < 50) || (valid_cfmt.parseInt > 150)) {
					validation_failed = true;
					//alert("The COND value is lower than 50 or higher than 150.");
				}		
			}
			//*/

			//alert("valid test 12");

			// ALTD
			var test_altd = IsNumeric(valid_altd);
			if (test_dbe == false) {
				validation_failed = true;
				//alert("The ALTD value is not numeric.");
			} else {

				if ((valid_altd.parseInt < 0) || (valid_altd.parseInt > 12000)) {
					validation_failed = true;
					//alert("The ALTD value is lower than zero or higher than 12,000.");
				}
			}

			//alert("valid test 13");

			// CFMT
			var test_cfmt = IsNumeric(valid_cfmt);
			if (test_dbe == false) {
				validation_failed = true;
				//alert("The CFMT value is not numeric.");
			} else {

				if ((valid_cfmt.parseInt < 0) || (valid_cfmt.parseInt > 1000000)) {
					validation_failed = true;
					//alert("The CFMT value is lower than zero or higher than 1,000,000.");
				}
			}

			//alert("valid test 14");

			//validate_ft(id_index);


			// CH (fin height) validation
			var test_ch = 0;
			var test_increment = 0;
			var test_result = 0;
			var ch_remainder = 0;

			if ((valid_ctyp == 5) || (valid_ctyp == 6)) {
				test_increment = 0.75;
			} else if ((valid_ctyp == 25) || (valid_ctyp == 35) || (valid_ctyp == 36) || (valid_ctyp == 37) || (valid_ctyp == 38)) {
				test_increment = 1;
			} else if ((valid_ctyp == 13)) {
				test_increment = 1.25;
			} else if ((valid_ctyp == 14) || (valid_ctyp == 15) || (valid_ctyp == 57)) {
				test_increment = 1.5;
			} else if ((valid_ctyp == 59)) {
				test_increment = 1.75;
			}

			//alert("valid test 15");

			test_result = valid_ch / test_increment;
			ch_remainder = valid_ch % test_increment;

			test_result = parseInt(test_result, 10);

			test_ch = test_result * test_increment;

			//alert("CH = " + valid_ch + "; after validation, CH = " + test_ch);

			valid_ch = test_ch;

			//alert("valid test 16");

			//*/


			// **************************
			// * /JAVASCRIPT VALIDATION * 
			// **************************



			//alert("valid test XYZ");

			// Return the values to the fields after validation
			//document.getElementById("tthelp" + id_index).value = valid_tthelp;


			// ***********************
			// * PHP TEMPLATE FILE 0 *
			// ***********************


			//alert("valid test 17");

			//*
			// If validation passed, submit the form
			if (validation_failed == false) {

				return true;

				// //alert("Validation passed! Please click 'OK' to run the calculations.");
				// document.getElementById("cc").submit();
			} else {
				return false;
			}
			//*/

			//alert("valid test 18");

		}

		// ***************************
		// * JS VALIDATION FUNCTIONS *
		// ***************************

		// Numeric check
		function IsNumeric(n) {
			return !isNaN(parseFloat(n)) && isFinite(n);
		}

		// Alphanumeric check
		function IsAlphaNumeric(input) {
			var TCode = input;
			if (/[^a-zA-Z0-9]/.test(TCode)) {
				return false;
			}
			return true;
		}
